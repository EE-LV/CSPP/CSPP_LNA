Readme
======
CSPP\_LNA extends the CSPP\_Core package of the **CS++** project. 

It contains CSPP\_LNAMessage.lvclass, a derived class of Message.lvclass

Refer to https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home for **CS++** project overview, details and documentation.

LabVIEW 2018 is the currently used development environment.

Related documents and information
---------------------------------
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : [https://git.gsi.de/EE-LV/CSPP/CSPP_LNA](https://git.gsi.de/EE-LV/CSPP/CSPP_LNA)
- Documentation:
  - Refer to package folder
  - Project-Wiki: https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
  - NI Actor Framework: https://.ni.com/actorframework

GIT Submodules
--------------
This package can be used as submodule

- Packages\CSPP_LNA:
  - CSPP_LNA.lvlib

External Dependencies
---------------------
- Linked Network Actor: [https://decibel.ni.com/content/docs/DOC-24051](https://decibel.ni.com/content/docs/DOC-24051) or use the VIPM

Getting started:
=================================
- Add CSPP_LNA.lvlib into your own LabVIEW project.
- Add CSPP\_LNAMessage.lvclass into your desired case of the CSPP\_UserContents.vi
- A dynamic dispatch _Parameter to Attribute.vi_ with variant input needs to be added to Message.lvclass in order to work properly. Refer to CSPP\_LNAMessage.lvclass:Do.vi for details.
- Message classes to be dispatched using CSPP\_LNAMessage.lvclass need to override _Parameter to Attribute.vi_ and parse the paramater variant attributes to its own object attibutes.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2013  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: https://eupl.eu/

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.